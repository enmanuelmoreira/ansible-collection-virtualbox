# Ansible Collection: VirtualBox

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-collection-virtualbox/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-collection-virtualbox/-/commits/main)
[![Galaxy Collection][badge-collection]][link-galaxy]

This role installs and configures VirtualBox hosts and guests.

## Requirements

- Ansible > 2.9 <= 4.10.0

## Installation

Install via Ansible Galaxy:

```bash
ansible-galaxy collection install enmanuelmoreira.virtualbox
```

Or include this collection in your playbook's requirements.yml file:

```bash
---
collections:
  - name: enmanuelmoreira.virtualbox
```

You also can download the collection using the following commands:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-collection-virtuabox
cd ansible-collection-virtualbox
git submodule update --init --recursive
```

You could modify the example playbook `playbook-example.yml` on the section roles to install the software as you wish.

And run the playbook:

```bash
ansible-playbook playbook-example.yml -K
```

## Dependencies

None.

## Example Playbook

    - hosts: localhost
      gather_facts: yes
      become: yes    
      
      roles:
        - virtualbox
        - virtualbox-guest

## License

MIT / BSD

[link-galaxy]: https://galaxy.ansible.com/enmanuelmoreira/virtualbox
[badge-collection]: https://img.shields.io/badge/collection-enmanuelmoreira.virtualbox-blue
